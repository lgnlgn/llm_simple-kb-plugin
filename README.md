# llm_simple-kb-plugin

#### 介绍
chatglm2外挂知识库的简单实现， 这是直接在web_demo2.py上修改的
详细介绍可看我写的[chatglm2外挂知识库问答的简单实现](https://blog.csdn.net/lgnlgn/article/details/131989563)

#### 软件架构



#### 安装教程

1. 需要安装了chatglm2, 可能chatglm也行
2. 需要安装text2vec和chromadb； 都可以用`pip install` 



#### 使用说明

1.  需要修改源码中的模型的路径 就是from_pretrain那个地址
2.  直接把  `texts` 和 `web_demo3.py` 放到目录里和web_demo.py一起 。
3.  随便找一些纯文本放在 `texts` 目录里，用`utf-8`编码。
4.  在你的chatglm2的使用环境中执行`streamlit run web_demo3.py`




